#!/usr/bin/env python
# coding: utf-8

# In[1]:


l=[30,40,50]
b=20
h=15
def area(dimensions): #Function to calculate area of largest side
    dimensions.sort(reverse=True)
    largest_side_area=dimensions[0]*dimensions[1]
    return largest_side_area

def volume(side1,side2,side3): #Function to calculate volume of the cuboid
    return (side1*side2*side3)

for i in l:
    print(f'Dimensions of the cuboid are:\n Length= {i} cm \n Breadth= {b} cm \n Height={h} cm\n')
    print(f'Area of one of the largest sides is:\n{area([i,b,h])} cm.sq.\nVolume of the cuboid is:\n{volume(i,b,h)} cm.cube\n\n')

